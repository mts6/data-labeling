<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Attribute\Route;

use function PHPUnit\Framework\fileExists;

class HomeController extends AbstractController
{
    const LABELS = [
        "uncategorized" => ["label" => "inconue", "src" => "https://r22er.com/wp-content/uploads/2021/01/Non-classe.png"],
        "chaise" => ["label" => "chaise", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/chaise-bistrot-en-bois-de-hetre-et-bambou-1000-8-34-230987_3.jpg"],
        "chaise-de-bureau" => ["label" => "chaise de bureau", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/chaise-de-bureau-pivotante-bouclettes-blanches-1000-16-7-218619_2.jpg"],
        "banc" => ["label" => "banc", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/banc-en-manguier-massif-et-metal-noir-mat-1000-13-26-198097_1.jpg"],
        "tabouret" => ["label" => "tabouret", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M21005117_4/tabouret-de-bar-en-acier-gris-et-bois-clair.jpg"],
        "banquette" => ["label" => "banquette", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/banquette-professionnelle-2-places-en-velours-recycle-taupe-1000-5-31-238651_1.jpg"],
        "fauteuil" => ["label" => "fauteuil", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/fauteuil-de-table-beige-chine-1000-1-5-234074_1.jpg"],
        "pouf" => ["label" => "pouf", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/pouf-tresse-imprime-geometrique-beige-et-marron-1000-2-6-226408_1.jpg"],
        "table" => ["label" => "table", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/table-a-manger-extensible-6-a-10-personnes-l160-240-1000-4-3-129850_4.jpg"],
        "table-basse" => ["label" => "table basse", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/table-basse-en-marbre-blanc-effet-travertin-et-bois-de-manguier-massif-1000-14-0-220334_1.jpg"],
        "table-de-bureau" => ["label" => "table de bureau", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M23166462_1/bureau-avec-2-tiroirs.jpg"],
        "table-coiffeuse" => ["label" => "table coiffeuse", "src" => "http://localhost:8000/assets/img/coiffeuse-effet-bois-acier-verre-marron-boise-et-noir.jpg"],
        "table-tv" => ["label" => "table tv", "src" => "http://localhost:8000/assets/img/meuble-tv-3-portes-en-chene-massif-clair.jpg"],
        "pied-de-table" => ["label" => "pied de table", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/pied-de-table-professionnel-en-metal-coloris-laiton-h73-1000-14-40-204507_1.jpg"],
        "plafonnier" => ["label" => "plafonnier", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/suspension-triple-en-bambou-et-metal-noir-1000-16-3-210374_1.jpg"],
        "canape" => ["label" => "canape", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/canape-2-3-places-en-velours-bleu-nuit-1000-8-40-209550_1.jpg"],
        "canape-d-angle" => ["label" => "canape d'angle", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/canape-d-angle-gauche-4-5-places-beige-chine-1000-15-20-210186_1.jpg"],
        "coussin" => ["label" => "coussin", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/coussin-en-rayonne-et-lin-brodes-ecrus-jaunes-terracotta-30x50-1000-14-3-209964_2.jpg"],
        "lampe" => ["label" => "lampe", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M23023455_1/lampe-a-poser-verre-petrole-et-blanc.jpg"],
        "lampadaire" => ["label" => "lampadaire", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M22134904_1/lampadaire-a-arc-en-acier-gris.jpg"],
        "armoire" => ["label" => "armoire", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/armoire-2-portes-1-tiroir-vert-kaki-1000-2-20-228431_9.jpg"],
        "buffet" => ["label" => "buffet", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/buffet-vintage-2-portes-3-tiroirs-1000-0-33-216308_2.jpg"],
        "commode" => ["label" => "commode", "src" => "http://localhost:8000/assets/img/commode-3-tiroirs-en-acacia-massif-350-11-2-199098_1.jpg"],
        "etagere" => ["label" => "étagère", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/etagere-industrielle-en-bois-de-sapin-massif-et-metal-noir-1000-5-22-216092_2.jpg"],
        "etagere-murale" => ["label" => "étagère murale", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/etagere-murale-destructuree-1000-12-37-199133_2.jpg"],
        "lit" => ["label" => "lit", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/lit-160x200-coloris-lin-1000-16-25-188461_2.jpg"],
        "berceau" => ["label" => "berceau", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M23172421_2/lit-bebe-evolutif-en-bois-blanc-avec-tiroir-120x60-cm.jpg"],
        "tete de lit" => ["label" => "tete de lit", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/tete-de-lit-vintage-170-1000-7-30-209294_2.jpg"],
        "sculpture" => ["label" => "sculpture", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M23130155_1/figurine-3-hippocampes-en-albasia.jpg"],
        "horloge" => ["label" => "horloge", "src" => "https://medias.maisonsdumonde.com/images/ar_1:1,c_pad,f_auto,q_auto,w_354/v1/mkp/M23127796_3/horloge-murale-ronde-d31-5cm-noir.jpg"],
        "porte-manteaux" => ["label" => "porte-manteaux", "src" => "https://medias.maisonsdumonde.com/image/upload/ar_1:1,c_fill,f_auto,q_auto,w_354/v1/img/patere-5-crochets-en-chene-et-metal-1000-9-5-189015_1.jpg"],
        "caisson" => ["label" => "caisson", "src" => "http://localhost:8000/assets/img/caisson-bureau-effet-bois-pvc-blanc.jpg"],
        "chilienne" => ["label" => "chilienne", "src" => "http://localhost:8000/assets/img/chilienne-en-bois-d-eucalyptus-et-toile-en-polyester-recycle-multicolore-1024-2-23-238274_1.jpg"],
        "mirroir" => ["label" => "mirroir", "src" => "http://localhost:8000/assets/img/grand-miroir-arche-117x170-350-3-32-239431_1.jpg"],
    ];

    const FOLDER_IMG_TO_CATEGORIZE = "images";

    private string $kernelDir;
    private string $imgDir;
    private string $imgDirCategorized;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernelDir = $kernel->getProjectDir();
        $this->imgDir = $this->kernelDir. "/public/". self::FOLDER_IMG_TO_CATEGORIZE;
        $this->imgDirCategorized = $this->kernelDir. "/public/categorized";
    }

    #[Route("/", name:"home_index")]
    public function index(): Response
    {
        $imgs = [];
        $max_count = 100;
        $iter = 0;
        foreach (scandir($this->imgDir) as $file) {
            if ($file !== '.' && $file !== '..') {
                $imgs[] = $file;
            }

            if ($iter > $max_count) {
                break;
            }
            $iter++;
        }
        
        return $this->render("home.html.twig", [
            "labels" => self::LABELS,
            "imgs" => $imgs,
            "imgFolder" => self::FOLDER_IMG_TO_CATEGORIZE,
        ]);
    }

    #[Route("/enregistrer", name: "save_category")]
    public function save(Request $request): RedirectResponse
    {
        $filesystem = new Filesystem();
        $nbrProduct = $request->get('nbrProduct', 0);
        $errors = [];
        
        for ($i = 0; $i < $nbrProduct; $i++) {

            $filename = $request->get('urlImg'. $i+1);
            $sourceFile = $this->imgDir. "/" .$filename;

            if (fileExists($sourceFile)) {
                $destinationFolder = $this->imgDirCategorized. "/" .$request->get('categoryImg'. $i+1);

                if (!$filesystem->exists($destinationFolder)) {
                    $filesystem->mkdir($destinationFolder);
                }

                try {
                    $filesystem->copy($sourceFile, $destinationFolder. "/" .$filename);
                    $filesystem->remove($sourceFile);
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }
            }
        }

        if (count($errors) > 0) {
            $this->addFlash("warning", $errors);
        } else {
            $this->addFlash("success", "OK");
        }
        
        return $this->redirectToRoute("home_index");
    }
}